# PRAJAK CHUNSAWAS 

Web Developer Backend, Frontend                                        
Contract: 083-2797494 | [softplus555@gmail.com](mailto:softplus555@gmail.com)  

>## Education
### BansomdejChaopraya Rajabhat University (2009-2014)
- Information Technology (Software Technology) 

>## Skills

```
JavaScript, Typescript, Vue JS, React JS, Node, PHP Laravel, 
VB, C#, CSS, HTML, Docker, MSSQL, MYSQL, JQuery
```

>## Work Experience

>### Freelance (2014-2016)
#### Web Game Online | Tech Stack - VueJS, PHP Laravel, MSSQL
- ระบบ สมัครสมาชิก, จัดอันดับ, กิจกรรม
- Truewallet API  
- Facebook API กิจกรรมรับรางวัล
- User Online 1000+

#### Web RigAlert Miner| Tech Stack - VueJS, PHP Laravel, C#, MYSQL
* ดูสถานะเครื่อง Miner เช่น GPU, CPU, Hash Power, Status จากหน้า Web, Line
* แจ้งเตือนผ่าน Line ตามที่กำหนดจาก Hash Power, GPU CPU Load Temp 
* สั่ง Reboot Shutdow ผ่าน Line

>### Outsource at Sansiri (2017-2020)
#### AgencyCommission | Tech Stack - VueJS, Node ExpressJS, MSSQL
* ระบบคำนวนค่า Commission เพื่อจ่าย Agency
#### StockMonitoring | Tech Stack - VueJS, Node ExpressJS, MSSQL
* ระบบ แสดงสถานะการ Booking Condo แบบ Realtime
#### SFDCApiUploadData | Tech Stack - VueJS, Node ExpressJS, Typescript, MSSQL
* ระบบ Upload ข้อมูล Customer, Booking อื่นๆ จาก DB ขึ้น Salesforce
#### TransferPlanning_MVC | Tech Stack - C# dot net, MSSQL
* ระบบ โอนบ้านคอนโด
#### CRM Report | Tech Stack - VB dot net, MSSQL
* ระบบออก Report PDF, รายงาน ต่างๆ
#### QuotationOnline | Tech Stack - VB, MSSQL
* ระบบสำหรับ การขาย Condo การออกใบเสนอราคา การทำจอง ทำสัญญาและเชื่อมต่อข้อมูลกับระบบ SAP
#### CRM | Tech Stack - VB, MSSQL
* ระบบสำหรับ การขาย บ้าน การออกใบเสนอราคา การทำจอง ทำสัญญาและเชื่อมต่อข้อมูลกับระบบ SAP
#### TrackQTReport | Tech Stack - VB, MSSQL
* ระบบสำหรับออก Report ต่างๆ 
#### Panda API (PDPA) | Tech Stack - Node, Typescript
* API เชื่อมต่อกับ Onetrust ใช้กับ PDPA
#### Octopus OTP | Tech Stack - Node, Typescript, Postgresql
* Send OTP ผ่าน SMS, Email
* Verify OTP
